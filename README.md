# Tarea_13_Ejercicios_JSX
## Descripcion

En este proyecto se encuentran los 13 ejercicios en JSX de manera individual.

## Como clonar
> Para clonar el proyecto brindamos las siguientes 2 opciones.
 * **Clone with SSH:** 
 ```
 git@gitlab.com:darias05/tarea_13_jsx_practica.git
 ```
 * **Clone with HTTPS:** 
 ```
 https://gitlab.com/darias05/tarea_13_jsx_practica.git
 ```

## Autores
> Dylan Arias Arenas

> Ginna Paola Tangarife